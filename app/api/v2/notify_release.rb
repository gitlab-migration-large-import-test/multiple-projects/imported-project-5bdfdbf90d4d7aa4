# frozen_string_literal: true

module V2
  class NotifyRelease < Grape::API
    include AuthenticationSetup

    desc "Update dependency" do
      detail "Trigger specific dependency update for given package ecosystem across all projects"
      success message: "Successfully triggered dependency update", examples: { dependency_update_triggered: true }
    end
    params do
      requires :dependency_name, type: String, desc: "Dependency name"
      requires :package_ecosystem, type: String, desc: "Package ecosystem"
    end
    post :notify_release do
      NotifyReleaseJob.perform_later(params[:dependency_name], params[:package_ecosystem])

      present({ dependency_update_triggered: true })
    end
  end
end
