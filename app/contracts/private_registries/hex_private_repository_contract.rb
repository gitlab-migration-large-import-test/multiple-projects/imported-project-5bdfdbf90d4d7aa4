# frozen_string_literal: true

module PrivateRegistries
  class HexPrivateRepositoryContract < Dry::Validation::Contract
    params do
      config.validate_keys = true

      required(:type).filled(:string)
      required(:repo).filled(:string)
      required(:url).filled(:string)
      required(:"auth-key").filled(:string)
      optional(:"public-key-fingerprint").filled(:string)
    end
  end
end
