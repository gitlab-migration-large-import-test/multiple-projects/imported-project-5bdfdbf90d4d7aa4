# frozen_string_literal: true

class NotifyReleaseJob < ApplicationJob
  queue_as :low

  sidekiq_options retry: false

  # Trigger package updates
  #
  # @param [String] name
  # @param [String] package_ecosystem
  # @return [void]
  def perform(dependency_name, package_ecosystem)
    Update::Routers::NotifyRelease.call(dependency_name, package_ecosystem)
  end
end
