# frozen_string_literal: true

class UpdateRunnerJob < ApplicationJob
  queue_as :default

  sidekiq_options retry: UpdaterConfig.job_retries

  # Trigger update runner
  #
  # @param [Hash] args
  # @return [void]
  def perform(args)
    project_name, package_ecosystem, directory = args.symbolize_keys.values_at(
      :project_name,
      :package_ecosystem,
      :directory
    )
    job_context = job_details(
      job: "dep-update",
      project_name: project_name,
      ecosystem: package_ecosystem,
      directory: directory
    )

    run_within_context(job_context) do
      Update::Routers::DependencyUpdate.call(project_name, package_ecosystem, directory)
    end
  end
end
