# frozen_string_literal: true

class Version
  VERSION = "3.6.0-alpha.1"
end
