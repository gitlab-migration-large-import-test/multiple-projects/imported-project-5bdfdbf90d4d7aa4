# Automatic rebase

Application supports automatically rebasing merge requests and automatically resolving conflicts if such are present. This behavior is configured by [rebase-strategy](../config/configuration.md#rebase-strategy) option.

## Deployed version

Automatic rebase functionality is further enhanced if application is used in deployed mode and has [webhooks](../config/webhooks.md) configured. When dependency update MR is merged, update of other open MR's of same package ecosystem is triggered according to configured strategy.

For standalone mode rebase/recreate action is performed only on next scheduled dependency update run.
