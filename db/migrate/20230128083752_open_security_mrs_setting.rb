# frozen_string_literal: true

class OpenSecurityMrsSetting < Mongoid::Migration
  def self.up
    Project.each do |project|
      project.configuration&.updates&.each { |conf| conf[:open_security_merge_requests_limit] = 10 }
      project.save!
    end
  end

  def self.down
    Project.each do |project|
      project.configuration&.updates&.each { |conf| conf.delete(:open_security_merge_requests_limit) }
      project.save!
    end
  end
end
