# frozen_string_literal: true

class RunStatus < Mongoid::Migration
  def self.up
    Update::Run.batch_size(1000).each do |run|
      run.update_attributes!(failed: run.failures.any?)
    end
  end

  def self.down; end
end
