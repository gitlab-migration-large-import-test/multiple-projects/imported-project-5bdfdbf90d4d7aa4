# frozen_string_literal: true

require_relative "util"

# Create release tag and update VERSION file
#
class ReleaseHelper
  private_instance_methods :new

  def initialize(version)
    @version_file_name = "app/services/version.rb"
    @compose_file_name = "docker-compose.yml"
    @version = version
  end

  # Update changelog and create new tag
  #
  # @param [String] version
  # @return [void]
  def self.call(version)
    new(version).call
  end

  # Update changelog and create new tag
  #
  # @param [String] version
  # @return [void]
  def call
    update_version
    commit_and_tag
    print_changelog
  end

  # Update changelog
  #
  # @return [void]
  def update_version
    logger.info("Updating version to #{ref_to}")
    logger.info("Updating version.rb and docker-compose.yml file")

    new_version = ref_to.to_s.delete("v")
    updated_version_rb = version_file_contents.gsub(ref_from, new_version)
    updated_compose_yml = compose_file_contents.gsub(ref_from, new_version)

    File.write(version_file_name, updated_version_rb, mode: "w")
    File.write(compose_file_name, updated_compose_yml, mode: "w")
  end

  # Commit update changelog and create tag
  #
  # @return [void]
  def commit_and_tag
    logger.info("Committing changes")

    git = Git.init
    git.add(version_file_name)
    git.add(compose_file_name)
    git.commit("Update app version to #{ref_to}", no_verify: true)

    logger.info("Creating release tag")
    git.add_tag(ref_to.to_s)
  end

  # Log changes in new release
  #
  # @return [void]
  def print_changelog
    changelog = gitlab
                .get_changelog("dependabot-gitlab/dependabot", ref_to, trailer: "changelog", from: "v#{ref_from}")
                .notes
    logger.info("Release contains following changes:\n#{changelog}")
  end

  private

  include Util

  attr_reader :version_file_name, :compose_file_name, :version

  delegate :parse, to: SemVer

  # Version file contents
  #
  # @return [String]
  def version_file_contents
    @version_file_contents ||= File.read(@version_file_name)
  end

  # Compose file contents
  #
  # @return [String]
  def compose_file_contents
    @compose_file_contents ||= File.read(compose_file_name)
  end

  # Current version
  #
  # @return [String]
  def ref_from
    @ref_from ||= version_file_contents.match(/VERSION = "#{Util::VERSION_REGEX}"/)[1]
  end

  # New version
  #
  # @return [SemVer]
  def ref_to
    send(version, ref_from)
  rescue NoMethodError
    parse(version)
  end

  # Update special version component
  #
  # @param [String] special
  # @return [String]
  def bump_special(special)
    return if special.blank?

    special.gsub(/\d+/) { |num| num.to_i.next }
  end

  # Reset special version component
  #
  # @param [String] special
  # @return [String]
  def reset_special(special)
    return if special.blank?

    special.gsub(/\d+/, "1")
  end

  # Increase patch version
  #
  # @param [String] ref_from
  # @return [SemVer]
  def patch(ref_from)
    parse(ref_from).tap do |ver|
      ver.patch += 1
      ver.special = reset_special(ver.special)
    end
  end

  # Increase minor version
  #
  # @param [String] ref_from
  # @return [SemVer]
  def minor(ref_from)
    parse(ref_from).tap do |ver|
      ver.minor += 1
      ver.patch = 0
      ver.special = reset_special(ver.special)
    end
  end

  # Increase major version
  #
  # @param [String] ref_from
  # @return [SemVer]
  def major(ref_from)
    parse(ref_from).tap do |ver|
      ver.major += 1
      ver.minor = 0
      ver.patch = 0
      ver.special = reset_special(ver.special)
    end
  end

  # Increase or set next major aplha version
  #
  # @param [String] ref_from
  # @return [SemVer]
  def pre(ref_from)
    parsed_ver = parse(ref_from)
    return major(ref_from).tap { |ver| ver.special = "alpha.1" } if parsed_ver.special.blank?

    parsed_ver.tap { |ver| ver.special = bump_special(ver.special) }
  end
end
